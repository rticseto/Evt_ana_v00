ROOTCFLAGS    = $(shell root-config --cflags)
ROOTLIBS      = $(shell root-config --libs) -lMLP -lTreePlayer -lMinuit
ROOTGLIBS     = $(shell root-config --glibs)
#TMVALIB	      = `pwd`/TMVA/lib/libTMVA.so
#TMVALIB	      = ./TMVA/lib/libTMVA.so
#TMVAFLAG      = -I./TMVA
#TMVAFLAG      = -I./TMVA/include/

CXX           = g++
CXXFLAGS      = -g -I$(ROOTSYS)/include -O -Wall -fPIC -lProof -lProofPlayer 
LD            = g++
SOFLAGS       = -shared


CXXFLAGS     += $(ROOTCFLAGS)
LIBS          = $(ROOTLIBS)
GLIBS         = $(ROOTGLIBS)

all: evt_ana

#AnaOBJ = lib/Particles.o lib/JEC.o lib/TES.o lib/evtsel.o lib/sel_empty.o lib/sel_Zb.o lib/sel_bjet_template.o lib/sel_cjet_template.o lib/sel_ljet_template.o lib/mc_jet_template.o lib/jet_ana.o 
#AnaOBJ = lib/topoline.o lib/Particles.o lib/JEC.o lib/TES.o lib/evtsel.o lib/sel_empty.o lib/sel_Zb.o lib/sel_Zbbar.o lib/sel_template.o lib/mc_jet_template.o lib/mc_line_eff.o lib/jet_ana.o lib/sel_Zbbar.o
AnaOBJ = lib/evtsel.o lib/evt_ana.o

evt_ana: $(AnaOBJ) evt_ana.exe

### SELECTION OBJECTS/EXECUTABLE ###

#lib/sel_empty.o: include/sel_empty.cpp include/sel_empty.h
#	$(CXX) -ggdb -c $(ROOTCFLAGS) $<
#	mv sel_empty.o lib/

#lib/mc_jet_template.o: include/mc_jet_template.cpp include/mc_jet_template.h
#	$(CXX) -ggdb -c $(ROOTCFLAGS) $<
#	mv mc_jet_template.o lib/

#lib/mc_line_eff.o: include/mc_line_eff.cpp include/mc_line_eff.h
#	$(CXX) -ggdb -c $(ROOTCFLAGS) $<
#	mv mc_line_eff.o lib/

#lib/sel_template.o: include/sel_template.cpp include/sel_template.h
#	$(CXX) -ggdb -c $(ROOTCFLAGS) $<
#	mv sel_template.o lib/

#lib/sel_ljet_template.o: include/sel_ljet_template.cpp include/sel_ljet_template.h
#	$(CXX) -ggdb -c $(ROOTCFLAGS) $<
#	mv sel_ljet_template.o lib/

#lib/sel_cjet_template.o: include/sel_cjet_template.cpp include/sel_cjet_template.h
#	$(CXX) -ggdb -c $(ROOTCFLAGS) $<
#	mv sel_cjet_template.o lib/

#lib/sel_bjet_template.o: include/sel_bjet_template.cpp include/sel_bjet_template.h
#	$(CXX) -ggdb -c $(ROOTCFLAGS) $<
#	mv sel_bjet_template.o lib/

#lib/sel_Zb.o: include/sel_Zb.cpp include/sel_Zb.h
#	$(CXX) -ggdb -c $(ROOTCFLAGS) $<
#	mv sel_Zb.o lib/ 

#lib/sel_Zbbar.o: include/sel_Zbbar.cpp include/sel_Zbbar.h
#	$(CXX) -ggdb -c $(ROOTCFLAGS) $<
#	mv sel_Zbbar.o lib/

lib/evtsel.o: include/evtsel.cxx include/evtsel.h
	$(CXX) -ggdb -c $(ROOTCFLAGS) $<
	mv evtsel.o lib/

#lib/TES.o: include/TES.cpp include/TES.h
#	$(CXX) -ggdb -c $(ROOTCFLAGS) $<
#	mv TES.o lib/

#lib/Particles.o: include/Particles.cpp include/Particles.h
#	$(CXX) -ggdb -c $(ROOTCFLAGS) $<
#	mv Particles.o lib/

#lib/topoline.o: include/topoline.cpp include/topoline.h
#	$(CXX) -ggdb -c $(ROOTCFLAGS) $<
#	mv topoline.o lib/

#lib/JEC.o: include/JEC.cpp include/JEC.h
#	$(CXX) -ggdb -c $(ROOTCFLAGS) $<
#	mv JEC.o lib/

#lib/ROOTOscar.g.o: include/Particles.cpp include/Particles.h
#	$(CXX) -ggdb -c $(ROOTCFLAGS) $<
#	mv ROOTOscar.g.o lib/

lib/evt_ana.o: include/evt_ana.cxx include/*
	$(CXX) -ggdb -c $(ROOTCFLAGS) $<
	mv evt_ana.o lib/

evt_ana.exe: $(AnaOBJ)
	$(CXX) -rdynamic -o evt_ana.exe $(AnaOBJ) $(ROOTLIBS) -l TMVA -L./lib64 -ldl

clean:
	rm -rf lib/*.o lib/*.so core* evt_ana*exe

