#include <stdio.h>
#include <vector>
#include "TFile.h"
#include <iostream>
//#include "TMVA/Reader.h"
//#include "TRFIOFile.h"
#include "evtsel.h"

using namespace std;

int main(int argc, char **argv){

   // Check that the input is correct
   if(argc != 5) {
      cerr << "Usage: " << argv[0] << " <configfile> <input> <output> <isMC> " << endl;
      cerr << "<configfile> = config file" << endl;
      cerr << "<input>      = input file" << endl;
      cerr << "<output>     = output file where the histos/trees will be written" << endl;
      cerr << "<MC/DATA>    = MC or DATA" << endl;
      return 1;
   }

   TString config(argv[1]);
   TString input(argv[2]);
   TString output(argv[3]);
   TString MC(argv[4]);
   Bool_t isMC;

   if(MC=="MC") isMC = 1; else if(MC=="DATA") isMC = 0; else {cerr << "<isMC> not set properly" << endl; exit(1);}

   cout << argv[0] << " will use:" << endl;
   cout << "<configfile> = " << config << endl;
   cout << "<input>      = " << input << endl;
   cout << "<output>     = " << output << endl;
   cout << "<isMC>       = ";
   if(isMC) { cout << "true"; } else { cout << "false"; }
   cout << endl;

   TFile *file = TFile::Open(input,"read");

   TFile *savefile = TFile::Open(output,"recreate");

   evtsel *mysel = new evtsel(config,file,savefile,isMC);
 
   cout << "Loop of events" << endl;

   mysel->loop();
   mysel->finish();

   savefile->cd();
   savefile->Write();

   file->Close();
   savefile->Close();
   file->Delete();
   savefile->Delete();

   cout << "\n##################################################" << endl << endl;
   cout << "\nThat's it." << endl << endl;
   cout << "\n##################################################" << endl << endl;
   return 1;
}

