#include "evtsel.h"
#include <stdlib.h>

using namespace std;

evtsel::evtsel(TString name, TFile *file, TFile *outputfile, int isMC) {

  TString treename = "truth";
//  if(!(TTree*)file->Get(treename)) treename = "truth";
  if(!(TTree*)file->Get(treename)) {cout << "Did not find TTree" << endl; exit(-1);}
  _tree = (TTree*)file->Get(treename);
  _entries = _tree->GetEntries();
  _debug = 5;
  _isMC = isMC;
  _type = 0;
  if(_isMC) { cout << "Monte Carlo" << endl; }
  else { cout << "DATA" << endl; }
  ReadConfigFile(name);
  SetTree();
  if(_debug>3) cout << "END: evtsel::evtsel()" << endl;
//  if(_debug==0) cout << "type"<<_type<< endl;
};

void evtsel::SetTree() {

  //Define the histograms

 

  hist1 = new TH1F("mc_b_from_t_pt","mc_b_from_t_pt",100,0,800);
  hist2 = new TH1F("mc_b_from_t_eta","mc_b_from_t_eta",20,-5.,5);
  hist3 = new TH1F("mc_b_from_t_phi","mc_b_from_t_phi",50,-4.,4);
  hist4 = new TH1F("mc_b_from_t_m","mc_b_from_t_m",100,0.,8000);

if(_type == 0) {  
  Sig = new TTree("Sig","Sig_2b");
//  Bkg = new TTree("Bkg","Bkg_1b");

  Sig->Branch("weight_mc", &weight_mc_t);
  Sig->Branch("eventNumber", &eventNumber_t);                           
  Sig->Branch("runNumber", &runNumber_t);
  Sig->Branch("mu", &mu_t);                           

}
  if(_debug>3) cout << "setting tree" << endl;

  _tree->SetBranchStatus("*",0);    
  setbranch("weight_mc", weight_mc);
  setbranch("eventNumber", eventNumber);
  setbranch("runNumber",runNumber );
  setbranch("mu", mu);
  setbranch("MC_b_from_t_pt", MC_b_from_t_pt ); 
  setbranch("MC_b_from_t_eta", MC_b_from_t_eta);
  setbranch("MC_b_from_t_phi", MC_b_from_t_phi);
  setbranch("MC_b_from_t_m", MC_b_from_t_m);
            
}; 

void evtsel::GetEntry(int i) {
   int begin_time = 0;
   if(i==0)int begin_time = clock()/CLOCKS_PER_SEC;
   if(!_tree) { cout << "No Tree!" << endl; }
   if(i%(_entries/100)==0) {
   //if( i % 10000 == 0) {
	   int time_elapsed = (clock()/CLOCKS_PER_SEC) - begin_time ;
	   float time_per_percent = 100.01*i/_entries > 0 ? time_elapsed / (100.01*i/_entries) : 1e-10;
	   cout << "\rProcessed " << i << " events or "  << int(100.01*i/_entries)  << " %  Elapsed time: " << time_elapsed << " Time per percent : " << time_per_percent << " Estimated time left(min): " << time_per_percent * (100 - int(100.01*i/_entries)) /60. << flush;
   }
   _tree->GetEntry(i);

};

bool evtsel::loop() {

  Long64_t entries = _tree->GetEntries();
  cout << "Tree has " << entries << " entries" << endl;
  //if(_events>0){ cout << "Number of events limited to " << _events << endl; system("time");}
  //
  float ljpt = 0.;
  float wjpt = 0.;
  for(int j=0;j<entries;j++) {
    
    if(_events>0 && j>=_events) break;
    if(_debug>1) { cout << endl << "Entry " << j << endl; }
    GetEntry(j);
    //////////////***/////////////
   ////////////////fill Tree.root /////////////////////////////
    if(_type==0) {

      weight_mc_t           = weight_mc;
      eventNumber_t         = eventNumber;
      runNumber_t           = runNumber;
      mu_t                  = mu;
     
     Sig->Fill();
   //   }
   //   if( label==5 && nBvtx==1 ){
    }
  float mc_weights = weight_mc;
  /////////////////////// fill hists ////////////////////////
  hist1->Fill(MC_b_from_t_pt/1000., mc_weights);
  hist2->Fill(MC_b_from_t_eta, mc_weights);
  hist3->Fill(MC_b_from_t_phi, mc_weights);
  hist4->Fill(MC_b_from_t_m, mc_weights);

  ////////////////////////////////////////////////////////////

  }
  
  return true; 
};


void evtsel::finish() {
   _tree->Delete();
 if(_type==0){
   Sig->Write();
  // Bkg->Write();
 }
// if(_type==1) treeBDT->Write();
    
};

void evtsel::ReadConfigFile(TString name)
{
  TEnv *base = new TEnv("./configfiles/evtsel_base.cfg");
  TEnv *env = new TEnv(name);
//Note: all min values should be -1e8 and max values 1e8

  _debug          		= env->GetValue("Debug", base->GetValue("Debug", 0));
  _events         		= env->GetValue("Events", base->GetValue("Events",-1));
  _type         		= env->GetValue("Type", base->GetValue("Type", 0));

  env->Print();

}; //ReadConfigFile
