#ifndef evtsel_h
#define evtsel_h
 
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <string>
#include <assert.h>
#include "TString.h"
#include "TObject.h"
#include "TObjString.h"
#include "TLeaf.h"
#include "TFile.h"
#include "TTree.h"
#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include "TDirectory.h"
#include "TRandom3.h"
#include "TEnv.h"
#include "TVector3.h"
#include "Rtypes.h"
#include "TRegexp.h"
#include "TList.h"
#include "TMath.h"
#include "TEfficiency.h"
#include "TLegend.h"
#include "TGraphAsymmErrors.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TPaveStats.h"
#include "TLine.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TLorentzVector.h"
#include <vector>
#include <utility>
#include <map>
#include <set>
#include "TMVA/Reader.h"
#include "TMVA/Tools.h"
//Selections

//#include "sel_Zbbar.h"

//MC Things

using namespace std;
//namespace TMVA { class Reader; };
class evtsel {

protected:

//Sel_Zbbar *sel_zbbar;


 public:

  evtsel(){};
  ~evtsel(){}

//  TES *EVENT;

  TTree 	*_tree;
  Int_t  	_type;
  Int_t		_debug;
  Int_t		_events;
  Bool_t	_isMC;
  Long64_t     _entries;

  

evtsel(TString name, TFile *file, TFile *outputfile, int isMC);

bool loop();

void finish();

bool passVarCut(Float_t min, Float_t max, Float_t var, TString varname); //passVarCut

bool passRequirement(bool isreq, bool ishas, Float_t weight, TString reqname); //passRequirement

bool passVeto(bool isveto, bool ishas, Float_t weight, Float_t rnd, TString vetoname); //passVeto

void ReadConfigFile(TString name);

  void GetEntry(int i);
 // void FillVars_EVENT();
  void SetTree();

void setbranch(const char *name,int &var)
{
  if(_debug>3) cout << "setting branch "  << name << endl;
  _tree->SetBranchStatus(name,1);
  _tree->SetBranchAddress(name,&var);
};

void setbranch(const char *name,UInt_t &var)
{
  if(_debug>3) cout << "setting branch "  << name << endl;
  _tree->SetBranchStatus(name,1);
  _tree->SetBranchAddress(name,&var);
};

void setbranch(const char *name,ULong64_t &var)
{
  if(_debug>3) cout << "setting branch "  << name << endl;
  _tree->SetBranchStatus(name,1);
  _tree->SetBranchAddress(name,&var);
};

void setbranch(const char *name,bool &var)
{
  if(_debug>3) cout << "setting branch "  << name << endl;
  _tree->SetBranchStatus(name,1);
  _tree->SetBranchAddress(name,&var);
};

void setbranch(const char *name,float &var)
{
  if(_debug>3) cout << "setting branch "  << name << endl;
  _tree->SetBranchStatus(name,1);
  _tree->SetBranchAddress(name,&var);
};

void setbranch(const char *name,double &var)
{
  if(_debug>3) cout << "setting branch "  << name << endl;
  _tree->SetBranchStatus(name,1);
  _tree->SetBranchAddress(name,&var);
};

void setbranch(const char *name,float *var)
{
  if(_debug>3) cout << "setting branch "  << name << endl;
  _tree->SetBranchStatus(name,1);
  _tree->SetBranchAddress(name,var);
};
void setbranch(const char *name,double *var)
{
  if(_debug>3) cout << "setting branch "  << name << endl;
  _tree->SetBranchStatus(name,1);
  _tree->SetBranchAddress(name,var);
};
void setbranch(const char *name,int *var)
{
  if(_debug>3) cout << "setting branch "  << name << endl;
  _tree->SetBranchStatus(name,1);
  _tree->SetBranchAddress(name,var);
};
void setbranch(const char *bname, const char *name,float &var)
{
  if(_debug>3) cout << "setting branch "  << name << endl;
  _tree->SetBranchStatus(bname,1);
  TLeaf *l = _tree->GetLeaf(name);
  l->SetAddress(&var);
};

void setbranch(const char *bname, vector<float> *var)
{
  if(_debug>3) cout << "setting branch "  << bname << endl;
  _tree->SetBranchStatus(bname,1);
  _tree->SetBranchAddress(bname,&var);
};
 private:
//  TMVA::Reader *reader;
  TTree *_intree; //TTree

  Float_t weight_mc;
  ULong64_t eventNumber;
  UInt_t runNumber;
  Float_t mu;

  Float_t MC_b_from_t_pt;
  Float_t MC_b_from_t_eta;
  Float_t MC_b_from_t_phi;
  Float_t MC_b_from_t_m;


  



 

  //List of histograms pointers
  TH1F* _hg[35][3]; 
  TH1F* _vxtype[13*3][3];
  TH1F* _hvar[13*3][3];
  TH2F* _hh[10][3];

  TH1F * hist1;
  TH1F * hist2;
  TH1F * hist3;
  TH1F * hist4;

  TTree* Sig ;
  TTree* Bkg;

  Float_t weight_mc_t;
  Float_t eventNumber_t; 
  Float_t runNumber_t;
  Float_t mu_t; 
 

  Float_t weight1;

 //

};
///////////////////////////////////////////
#endif //evtsel_HPP_
